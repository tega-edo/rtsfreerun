# rtsfreerun
Free-running advligorts models in python

## Setup
- Debian buster or other linux OS with gcc (support for clang/macos is experimental)
- `apt install python3 python3-pip python3-dev python3-cffi python3-numpy git perl libswitch-perl build-essential`
- Remove any pre-existing contents of `userapps`
- Place in `userapps` the `.mdl` file to be built freerunning, and any dependencies
- `pip3 install .`

## Usage example
```python
import numpy as np
import scipy.signal as sig
import matplotlib.pyplot as plt
import x1sysexample

mdl = x1sysexample.x1sysexample()
duration = 10  # sec

# setting epics variables
mdl.write('SYS-EXAMPLE_CTRL_DAMP_GAIN', 1)
fm = mdl.LIGOFilter('SYS-EXAMPLE_CTRL_DAMP')
fm.turn_off('INPUT')
# loading filters
coef = sig.butter(4, 0.1, btype='low', output='sos', fs=mdl.sample_rate)
mdl.fm_set_sos('EXAMPLE_CTRL_DAMP', 'FM1', coef)
# running excitations (not implemented yet; use mdl.run() interface instead)
#exc = mdl.Sine('SYS-EXAMPLE_CTRL_DAMP_EXC', freq=1, ampl=1)
#exc.start()
# acquiring data (runs the model)
buffers = mdl.fetch(0, duration, ['SYS-EXAMPLE_CTRL_DAMP_OUT'])
#exc.stop()
plt.plot(buffers[0].data)
plt.show()
```

## Limitations
- Only one model can be imported and run per python process
