#!/usr/bin/env python3

import os
import sys
import glob
import pathlib
import shutil

from setuptools import setup

os.chdir(os.path.dirname(sys.argv[0]) or '.')

if 'model' in os.environ:
    model = os.environ['model']
else:
    cwd = pathlib.Path.cwd()
    userapps = f'{cwd}/userapps'
    model = glob.glob(f'{userapps}/*.mdl')[0]
    model = pathlib.Path(model).stem
    os.environ['model'] = model

setup(
    name=model,
    version='0.0.1',
    url='https://git.ligo.org/christopher.wipf/rtsfreerun',
    author='Christopher Wipf',
    author_email='christopher.wipf@ligo.org',
    packages=[f'{model}'],
    package_dir={'': 'wrap_out'},
    package_data={'': ['*.txt']},
    install_requires=['cffi>=1.0.0', 'numpy', 'scipy'],
    setup_requires=['cffi>=1.0.0'],
    cffi_modules=['build_freerun.py:ffibuilder']
)

