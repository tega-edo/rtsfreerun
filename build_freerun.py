#!/usr/bin/env python3

import sys
import os
import pathlib
import shutil
import glob
import subprocess
import cffi

cwd = pathlib.Path.cwd()
userapps = f'{cwd}/userapps'

model = os.environ['model']
print(f'building freerunning {model}')

# generate python wrapper
shutil.rmtree(f'{cwd}/wrap_out', ignore_errors=True)
pathlib.Path(f'{cwd}/wrap_out').mkdir()
pathlib.Path(f'{cwd}/wrap_out/{model}').mkdir()
with open(f'{cwd}/wrap/__init__.template') as f:
    template = f.read()
template = template.replace('<<model>>', model)
with open(f'{cwd}/wrap_out/{model}/__init__.py', 'w') as f:
    f.write(template)

# generate C code
pathlib.Path(f'{userapps}/ccodeio.h').touch()
if not pathlib.Path(f'{cwd}/advligorts').exists():
    #subprocess.run(['git', 'clone', 'https://git.ligo.org/cds/advligorts.git'], check=True)
    # pin to an advligorts release tag (comment out the next line to build against master)
    #subprocess.run(['git', 'checkout', 'tags/4.2.7', '-b', '_build_rts_freerun'], cwd=f'{cwd}/advligorts', check=True)
    # use fork until fixes are merged
    subprocess.run(['git', 'clone', 'https://git.ligo.org/christopher.wipf/advligorts.git'], check=True)
    subprocess.run(['git', 'checkout', 'noise_ran'], cwd=f'{cwd}/advligorts', check=True)
env = os.environ.copy()
env.setdefault('SITE', 'TST')
env.setdefault('site', 'tst')
env.setdefault('IFO', 'X1')
env.setdefault('ifo', 'x1')
env.setdefault('RCG_LIB_PATH', userapps)
env.setdefault('CDS_SRC', userapps)
env.setdefault('CDS_IFO_SRC', userapps)
env.setdefault('RCG_SRC_DIR', f'{cwd}/advligorts')
env.setdefault('PERL5LIB', f'{cwd}/advligorts/src/epics/util')
subprocess.run(['./feCodeGen.pl', f'{model}.mdl', f'{model}'], cwd=f'{cwd}/advligorts/src/epics/util', env=env, check=True)

# copy filter file for inclusion in package data
shutil.copy(f'{cwd}/advligorts/build/{model}epics/config/{env["IFO"]}{model.upper()}.txt', f'{cwd}/wrap_out/{model}/{model.upper()}.txt')

ffibuilder = cffi.FFI()

with open(f'{cwd}/advligorts/src/fe/{model}_usp/{model}.c') as f:
    model_lines = f.readlines()
# omit unneeded controller code
model_lines = [line for line in model_lines
               if not ('#include' in line and 'controller' in line)]
model_source = ''.join(model_lines)

# global model state variable declarations
model_vars = []
skipped_lines = []
for n, line in enumerate(model_lines):
    if n in skipped_lines:
        continue
    if n == 0:
        # start collecting vars at end of CDS_CARDS array
        skipped_lines.append(n)
        n += 1
        while not model_lines[n].startswith('};'):
            skipped_lines.append(n)
            n += 1
        skipped_lines.append(n)
        continue
    if 'feCode' in line:
        # stop collecting vars at start of feCode definition
        break
    # skip RNG functions
    if 'noise_doub' in line:
        skipped_lines.append(n)
        continue
    if 'noise_int64' in line or 'noise_ran' in line:
        skipped_lines.append(n)
        n += 1
        while not model_lines[n].startswith('}'):
            skipped_lines.append(n)
            n += 1
        skipped_lines.append(n)
        continue
    # skip non-inlined function declarations
    if line.startswith('#include'):
        skipped_lines.append(n)
        continue
    if '=' in line:
        line = f"{line.split('=')[0]};\n"
    if line.strip():
        line = f'extern {line}'
    model_vars.append(line)
model_vars = ''.join(model_vars)

# from feuser.h (most are not needed by feCode)
externs = '''
int          cycleNum;
unsigned int odcStateWord;
unsigned int cycle_gps_time;
unsigned int dWordUsed[ MAX_ADC_MODULES ][ 32 ];
unsigned int dacOutUsed[ MAX_DAC_MODULES ][ 16 ];
int          dacChanErr[ MAX_DAC_MODULES ];
unsigned int CDIO6464Output[ MAX_DIO_MODULES ];
unsigned int CDIO1616Output[ MAX_DIO_MODULES ];
unsigned int CDIO1616InputInput[ MAX_DIO_MODULES ];
unsigned int CDIO6464InputInput[ MAX_DIO_MODULES ];
double*      testpoint[ 500 ];
double       xExc[ 50 ];
char*        _shmipc_shm;
int          startGpsTime;
CDS_HARDWARE cdsPciModules;
unsigned int ipcErrBits;
unsigned int timeSec;
unsigned int curDaqBlockSize;
'''

model_source += externs

helper_source = r'''
int need_init = 1;

void run(int cycles,
         FILT_MOD *dsp_ptr,      /* Filter Mod variables */
         COEF *dspCoeff,         /* Filter Mod coeffs */
         CDS_EPICS *pLocalEpics, /* EPICS variables */
         int num_testpoints,
         double **testpoints,
         double *testpoint_data,
         int num_excitations,
         double **excitations,
         double *excitation_data) {
    double dWord[8][32];
    double dacOut[8][16];

    if (need_init) {
        feCode(cycleNum, dWord, dacOut, dsp_ptr, dspCoeff, pLocalEpics, need_init);
        need_init = 0;
    }

    for (int i = 0; i < cycles; i++) {
        for (int j = 0; j < num_excitations; j++) {
            *(excitations[j]) = excitation_data[i*num_excitations+j];
        }
        feCode(cycleNum, dWord, dacOut, dsp_ptr, dspCoeff, pLocalEpics, need_init);
        for (int j = 0; j < num_testpoints; j++) {
            testpoint_data[i*num_testpoints+j] = *(testpoints[j]);
        }
        cycleNum++;
    }
}
'''

model_source += helper_source

with open(f'{cwd}/advligorts/src/fe/{model}_usp/Makefile') as f:
    cflags_lines = f.readlines()
cflags_lines = [line for line in cflags_lines
                if line.startswith('CFLAGS')]

cflags = []
for line in cflags_lines:
    flag = line.split('+=')[1].strip()
    flag = flag.replace('\\"', '"')
    cflags += flag.split(' ')

ffibuilder.set_source(f'_{model}', model_source, extra_compile_args=cflags)

with open(f'{cwd}/advligorts/src/include/{model}.h') as f:
    epics_lines = f.readlines()
# mangle header to satisfy limitations of cffi cdef
# perhaps this header can be split into more easily digestible parts
epics_cdef = [line for line in epics_lines
              if not line.startswith(('#ifndef', '#endif', f'#define {model.upper()}', '#define TARGET', '#define SYSTEM'))]
epics_cdef = ''.join(epics_cdef)
ffibuilder.cdef(epics_cdef)

fm_cdef = []
with open(f'{cwd}/advligorts/src/include/fm10Gen.h') as f:
    fm_lines = f.readlines()
# mangle header to satisfy limitations of cffi cdef
# perhaps this header can be split into more easily digestible parts
skipped_lines = []
for n, line in enumerate(fm_lines):
    if n in skipped_lines:
        continue
    if 'FIR_FILTERS' in line or 'NO_FM10GEN_C_CODE' in line:
        while not fm_lines[n].startswith('#endif'):
            skipped_lines.append(n)
            n += 1
        continue
    if line.startswith(('#if', '#endif', '#include', '#define FM10GEN_H', '#define UINT32')):
        skipped_lines.append(n)
        continue
    line = line.replace('UINT32', 'unsigned int')
    fm_cdef.append(line)
fm_cdef = ''.join(fm_cdef)
ffibuilder.cdef(fm_cdef)

with open(f'{cwd}/advligorts/src/include/tRamp.h') as f:
    tRamp_lines = f.readlines()
tRamp_cdef = [line for line in tRamp_lines
              if not line.startswith(('#ifndef', '#endif', '#include', '#define TRAMP_H'))]
tRamp_cdef = ''.join(tRamp_cdef)
ffibuilder.cdef(tRamp_cdef)

ffibuilder.cdef(model_vars)

feCode_cdef = '''
#define FE_RATE ...
/* Switching register bits */
#define OPSWITCH_INPUT_ENABLE ...
#define OPSWITCH_OFFSET_ENABLE ...
#define OPSWITCH_LIMITER_ENABLE ...
#define OPSWITCH_DECIMATE_ENABLE ...
#define OPSWITCH_OUTPUT_ENABLE ...
#define OPSWITCH_HOLD_ENABLE ...

#define OPSWITCH_LIMITER_RAMPING ...
#define OPSWITCH_GAIN_RAMPING    ...

extern const unsigned int pow2_in[ 10 ]; /* UINT32 */

extern int          cycleNum;
extern double*      testpoint[ 500 ];
extern double       xExc[ 50 ];

extern RampParamState gain_ramp[ MAX_MODULES ][ 10 ];
extern RampParamState offset_ramp[ MAX_MODULES ][ 10 ];
void RampParamInit( RampParamState* state, double xInit, const int fe_rate );

extern int need_init;

void run(int cycles,
         FILT_MOD *dsp_ptr,      /* Filter Mod variables */
         COEF *dspCoeff,         /* Filter Mod coeffs */
         CDS_EPICS *pLocalEpics, /* EPICS variables */
         int num_testpoints,
         double **testpoints,
         double *testpoint_data,
         int num_excitations,
         double **excitations,
         double *excitation_data);
'''
ffibuilder.cdef(feCode_cdef)


if __name__ == '__main__':  # not when running with setuptools
    output_filename = ffibuilder.compile(verbose=True)
    print(f'built {output_filename}')

